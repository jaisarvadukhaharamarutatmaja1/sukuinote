# Sukuinote

A helping hand that has multiple accounts and a slave attached. Original repo:https://www.gitlab.com/blankX/sukuinote. 
I did my part and thus this edited readme. I added the Heroku part :).

### Installation Instructions
1. Install:
    - `python3` (this is in python after all)
    - `ffmpeg` (to get the first frame of GIFs/Videos in WhatAnime)
2. `pip3 install -r requirements.txt`
3. Copy example-config.yaml to config.yaml and edit it
4. `mkdir sessions`
NOTE: This is through terminal but will use ur own bandwidth. To save ur data, use it to deploy through heroku. 
### Start
`python3 -m sukuinote`  
After that, send .help somewhere and have fun :D

### Through heroku.
1. Clone and go into the directory. Rest, google it as I did.  Use heroku cli to clone the repository and push your changes eachtime as in installation instructions.
2. `git push heroku master`
3. You can add ffmpeg using buildpacks. Visit heroku for that. Also, python build pack must have a runtime >= 3.9.
4. Scale the dynos.
5. `heroku run bash`
6. Your Process must have a worker Procfile wherein it is written what is to be done. Also, copy __init__.py to outside to run it.
7. `mkdir sessions`
8. In heroku run bash python -m sukuinote 
9. Login get your values.
10. Go to Sessions and download the files and push them to the repostiory.
11. All I could help is done, any more errors-> stackoverflow as I did.



